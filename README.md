## Minha configuracao gitconfig

- Arquivo **/home/$USER/.gitconfig**

```bash

# diretório onde ficarão os projetos
# hospedados no codeberg
[includeIf "gitdir:~/codeberg/**"]
  path = ~/codeberg/gitconfig

# hospedados no github
[includeIf "gitdir:~/github/**"]
  path = ~/github/gitconfig

# projetos hospedados no gitlab claretiano
[includeIf "gitdir:~/claretiano/production/**"]
  path = ~/claretiano/production/gitconfig


```

## Config com email pessoal

- Diretórios de projetos pessoais **/home/$USER/codeberg | /home/$USER/github**

```bash
[init]
	defaultBranch = master
[color]
	ui = true
[user]
	name = Jesher Minelli
	email = jesherdevsk8@gmail.com
[cola]
	startupmode = folder
	spellcheck = false

```

## Config com email de trabalho

- Diretório dos projetos do trabalho **/home/$USER/claretiano/production**

```bash

[init]
	defaultBranch = master
[color]
	ui = true
[user]
	name = Jesher Minelli
	email = jesheralves@claretiano.edu.br
[cola]
	startupmode = folder
	spellcheck = false

```
